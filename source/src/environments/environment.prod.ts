export const environment = {
    apiBaseUrl: 'http://tchallenge-service-production.openshift.devops.t-systems.ru',
    clientBaseUrl: 'http://localhost:4200',
    production: true
};
