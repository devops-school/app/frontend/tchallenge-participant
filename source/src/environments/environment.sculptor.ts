export const environment = {
    apiBaseUrl: 'http://tchallenge-service-sculptor.openshift.devops.t-systems.ru',
    clientBaseUrl: 'http://localhost:4200',
    production: false
};
